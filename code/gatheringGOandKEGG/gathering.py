from strop import count

import requests
from time import sleep
import re

file1 = open('F:\\Project\\payanName\\dataset\\allProteins.txt', 'r')
file2 = open('F:\\Project\\payanName\\dataset\\allDataOfGenGoKEGG1.txt', 'a')
count = 1
nodes = []
while True:

    # Get next line from file
    line = file1.readline()
    spreadLine = line.split("\n")

    # if line is empty
    # end of file is reached
    if not line:
        break

    if count > 11260:
        x = requests.get("https://string-db.org/api/json/enrichment?identifiers=" + spreadLine[0])
        print(spreadLine[0])
        if x.status_code == 200:
            file2.write(spreadLine[0] + "/")
            file2.write("GO")
            for i in x.json():
                if len(i) > 0:
                    if i["category"] == "Function":
                        file2.write("," + i["term"])
                    elif i["category"] == "Process":
                        file2.write("," + i["term"])
                    elif i["category"] == "Component":
                        file2.write("," + i["term"])

            file2.write("/KEGG")
            if len(i) > 0:
                for i in x.json():
                    if i["category"] == "KEGG":
                        file2.write("," + i["term"])

            file2.write("\n")

    count = count + 1

file1.close()
file2.close()

readable = 1
count = 0
goTerm = ""
goCat = ""
goComment = ""
genes = [];
x = open("F:\Project\payanName\dataset\curated_gene_disease_associations.tsv", 'r')
y = open("F:\Project\payanName\dataset\ParkinsonDisease.txt", 'a')

while True:
    line = x.readline()
    lineSeprated = []
    if not line:
        break

    line = x.readline()
    lineSeprated = line.split('\t')
    # if count > 10:
    #     break

    if len(lineSeprated) > 5 and "Parkinson Disease" in lineSeprated[5]:
        print(lineSeprated)

        if lineSeprated[1] not in genes:
            genes.append(lineSeprated[1])
            count = count + 1
        # goTerm = lineSeprated[1]+":"+lineSeprated[2] + ","
        # y.write(goTerm.rstrip())
        # line = x.readline()
        # lineSeprated = line.split(':')
        # print (line)
        # goComment = lineSeprated[1]
        #
        # line = x.readline()
        # lineSeprated = line.split(':')
        # print (line)
        # goCat = lineSeprated[1]
        # y.write(goCat.rstrip() + ",")
        # y.write(goComment.rstrip() + "\n")
        #

for i in genes:
    y.write(i + "\n")

y.close()

keggIdList = []
f = open("F:\\Project\\payanName\\dataset\\ids.txt", 'r')

f2 = open("F:\\Project\\payanName\\dataset\\allGoandKEGGIdsWithNodes.txt", 'a')
count = 0
writeIsOK = True
newLine = False
while True:

    line = f.readline()
    number = 0
    if not line:
        break

    spLine = line.split(",")

    f1 = open("F:\\Project\\payanName\\dataset\\allDataOfGenGoKEGG.txt", 'r')
    while True:

        line1 = f1.readline()
        if not line1:
            break

        spLine1 = line1.split("/")

        if spLine[0].split("\n")[0] in spLine1[1]:
            if writeIsOK == True:
                f2.write(spLine[0].split("\n")[0])
                writeIsOK = False
            f2.write("," + spLine1[0])
            newLine = True
            number = number + 1
        if spLine[0].split("\n")[0] in spLine1[2]:
            if writeIsOK == True:
                f2.write(spLine[0].split("\n")[0])
                writeIsOK = False
            f2.write("," + spLine1[0])
            newLine = True
            number = number + 1

    writeIsOK = True
    if newLine == True:
        f2.write("\n")
        newLine = False

f2.close()
f1.close()
f.close()
#
# f = open("F:\\Project\\payanName\\dataset\\curated_gene_disease_associations.tsv", 'r')
# f1 = open("F:\\Project\\payanName\\dataset\\AlzheimerDisease", 'a')
# count = 1
# while True:
#     line = f.readline()
#     if not line:
#         break
#
#     lsp = line.split("\t")
#     print(lsp)
#     if "Alzheimer's Disease" in lsp[5]:
#         f1.write(lsp[1] + '\n')
#
#     count = count + 1
#
#     # if count > 10:
#     #     break
#
#
# f.close()
# f1.close()
