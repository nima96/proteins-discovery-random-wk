import numpy as np
from operator import itemgetter
import sys


def get_item_I(arr, item):
    # unique item
    return np.where(np.asarray(arr) == item)[0][0]


def key_sorted_by_val(d, lowToHigh=False):
    if lowToHigh:
        sortedD = sorted(d.items(), key=itemgetter(1))
    else:
        sortedD = sorted(d.items(), key=itemgetter(1), reverse=True)
    sortedKey = [k for k, _ in sortedD]
    return sortedKey


def sort_key_val(keys, vals, lowToHigh=False):
    valsMap = {}
    for i in range(0, len(vals)): valsMap[i] = vals[i]
    sortedMap = key_sorted_by_val(valsMap)
    sortedKeys = [keys[i] for i in sortedMap]
    sortedVals = [vals[i] for i in sortedMap]
    return sortedKeys, sortedVals


class Helper:
    def pathNotTraveled(paths, path):
        for i in paths:
            if "".join([str(j) for j in path]) in "".join([str(j) for j in i]): return False
        return True

    def isNotCycle(path, node):
        return not node in path

    def edgeStr(nodeArr):
        # OBSELETE, BECAUSE SOME GENE HAS A SYMBOL OF "-"
        return str(nodeArr[0]) + "-" + str(nodeArr[1])

    def item_in_set(i, S):
        for j in i:
            if j in S: return True
        return False

    def is_subset(subS, S):
        count = 0
        for i in subS:
            if i in S: count += 1
        if count == len(subS): return True
        return False

    def brsEntry_to_Str(brs):
        brStr = []
        for br in brs:
            brStr.append([str(node) for node in br])
        return brStr

    def binary_relation_to_node(binary_relation):
        return set(list(np.unique(np.asarray(binary_relation).flatten())))

    def pathStr(path):
        # given a list, serialize the list of elements as str, return a str
        return "\t".join([str(i) for i in path])

    def list_to_pathStrs(paths):
        return [Helper.pathStr(path) for path in paths]

    def pathStrs_to_list(pathStr, isInt=False):
        # given a list of str (the str is a serialized list), de-serialize them, return a list of lists
        if isInt:
            paths = [i.split("\t") for i in pathStr]
            intPaths = []
            for path in paths:
                intPaths.append([])
                for i in path: intPaths[-1].append(int(i))
            return intPaths
        else:
            return [i.split("\t") for i in pathStr]

    def br_to_pathStr(br):
        return "{}\t{}".format(br[0], br[1])

    def get_targets(binary_relation):
        source = [i[0] for i in binary_relation]
        target = [i[1] for i in binary_relation]
        leaf = [i for i in target if i not in source]
        return list(set(leaf))

    def get_sources(binary_relation):
        source = [i[0] for i in binary_relation]
        target = [i[1] for i in binary_relation]
        head = [i for i in source if i not in target]
        return list(set(head))

    def to_dual_binary_relation(binary_relation):
        binary_relation_plus = binary_relation.copy()
        for i in binary_relation:
            binary_relation_plus.append([i[1], i[0]])
        binary_relation_plus = Helper.list_to_pathStrs(binary_relation_plus)
        binary_relation_plus = list(set(binary_relation_plus))
        binary_relation_plus = Helper.pathStrs_to_list(binary_relation_plus)
        return binary_relation_plus

    def to_dual_relation(relation):
        relation_plus = relation.copy()
        for key, nodes in relation.items():
            for node in nodes:
                if node not in relation_plus:
                    relation_plus[node] = set([key])
                else:
                    relation_plus[node].add(key)
        return relation_plus

    # this is probably unstable
    # def dual_to_mono(brs):
    #     monoBR = []
    #     for br in brs:
    #         if br[1] < br[0]: monoBR.append([br[1], br[0]])
    #         else: monoBR.append([br[0], br[1]])
    #     monoBR = Helper.pathStrs_to_list(set(Helper.list_to_pathStrs(monoBR)))
    #     return monoBR

    def filter_self_cycle(brs):
        noCycleBR = []
        for br in brs:
            if br[0] == br[1]: continue
            noCycleBR.append([br[0], br[1]])
        return noCycleBR

    def binary_to_relation(binaryRelat, rSet=False):
        nodes = Helper.binary_relation_to_node(binaryRelat)
        # build dict
        relation = {}
        if rSet:
            for node in nodes: relation[node] = set()
            for nodeArr in binaryRelat: relation[nodeArr[0]].add(nodeArr[1])
        else:
            for node in nodes: relation[node] = []
            for nodeArr in binaryRelat: relation[nodeArr[0]].append(nodeArr[1])
        return relation

    def relation_to_binary(relation):
        br = []
        for key, arr in relation.items():
            for item in arr: br.append([key, item])
        return br

    def pathsEntry_to_geneName(entry, paths):
        namedPaths = []
        for path in paths:
            namedPaths.append([entry[node]["first_graphics"] for node in path])
        return namedPaths

    def paths_to_binary_relation(paths, dual=False):
        relation = []
        for path in paths:
            for node in range(0, len(path) - 1):
                if [path[node], path[node + 1]] not in relation:
                    relation.append([path[node], path[node + 1]])
                if [path[node + 1], path[node]] not in relation and dual:
                    relation.append([path[node + 1], path[node]])
        return relation

    def geneName_to_entry(entrys, geneName):
        ids = []
        for node in entrys:
            if entrys[node]["first_graphics"] == geneName: ids += [node]
        return ids

    def entry_to_geneName(entrys, entry):
        return entrys[entry]["first_graphics"]

