from itertools import combinations
import math

from l3.helper import Helper, sort_key_val


class ns:
    BRToRelat = Helper.binary_to_relation
    toDualBR = Helper.to_dual_binary_relation
    BRToNode = Helper.binary_relation_to_node
    arr_pStr = Helper.list_to_pathStrs
    pStr_arr = Helper.pathStrs_to_list
    br_str = Helper.br_to_pathStr


def L3_normalization(PPIr, nodeX, nodeY):
    uvPair, candidateUs, candidateVs = get_uv(nodeX, nodeY, PPIr)
    score = 0
    for uv in uvPair:
        if math.sqrt(len(PPIr[uv[0]]) * len(PPIr[uv[1]])) == 0: continue
        score += 1 / math.sqrt(len(PPIr[uv[0]]) * len(PPIr[uv[1]]))
    return score


def get_uv(x, y, PPIr):
    candidateUs = PPIr[x]
    candidateVs = PPIr[y]
    uvPair = []
    for u in candidateUs:
        for v in candidateVs:
            if u not in PPIr[v]: continue
            uvPair.append([u, v])
    return uvPair, candidateUs, candidateVs


def PPILinkPred(samplePPIbr):
    samplePPIr = ns.BRToRelat(ns.toDualBR(samplePPIbr), rSet=True)
    sampleNodes = ns.BRToNode(samplePPIbr)
    nodePairs = list(combinations(sampleNodes, 2))
    scores, predictedPPIbrs = [], []
    for nodePair in nodePairs:
        [nodeX, nodeY] = nodePair
        if nodeY in samplePPIr[nodeX]: continue
        score = L3_normalization(samplePPIr, nodeX, nodeY)
        scores.append(score)
        predictedPPIbrs.append(nodePair)
    sortedPPIbrs, sortedScores = sort_key_val(predictedPPIbrs, scores)
    return sortedPPIbrs, sortedScores
