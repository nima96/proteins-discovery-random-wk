import os

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pickle


class rwr:
    def __init__(self, gr: nx.Graph):
        self.gr = gr
        self._save_flag = True
        self.__PR__FILE = "../cache/page_ranks_list_data.data"

        if os.path.exists(self.__PR__FILE):
            self.load_graph()
        else:
            self._list_pages_ranks = []

    def get_lists(self):
        return self._list_pages_ranks

    def clear_list(self):
        self._list_pages_ranks = []

    def walk(self, star_nodes: np.ndarray):
        star_nodes = dict((k, 1) for k in star_nodes)
        print("node count ", len(list(self.gr.nodes)))
        try:
            pr = nx.pagerank_scipy(self.gr,
                                   max_iter=250,
                                   alpha=0.8,
                                   personalization=star_nodes,
                                   weight="weight")
            pr = dict(reversed(sorted(pr.items(), key=lambda item: item[1])))
            self._list_pages_ranks.append(pr)
            return pr
        except nx.exception.PowerIterationFailedConvergence:
            print("coverage error for ", star_nodes)

    def _save_(self):
        if self._save_flag:
            with open(self.__PR__FILE, 'wb') as handle:
                pickle.dump(self._list_pages_ranks, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def load_graph(self):
        with open(self.__PR__FILE, 'rb') as handle:
            self._list_pages_ranks = pickle.load(handle)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._save_()

    def filter_from_array(self, filter_arr):
        self._save_()
        self._list_pages_ranks = []
        all_nodes = set(list(self.gr.nodes))
        filter_ = set(filter_arr)

        un_valid_nodes = list(all_nodes - filter_)
        print(" un_valid_nodes size", len(un_valid_nodes))
        for unvaild in un_valid_nodes:
            self.gr.remove_node(unvaild)
        self._save_flag = False

    def showGraph(self):
        nx.draw(self.gr)
        plt.show()
