import os
import pickle


class Mis:
    def __init__(self):
        self._path_ = "../cache/mis_test.data"
        self.gene_value_pair = {}

    def cal(self, gene_filtered, parkinson, graphForm):
        if os.path.exists(self._path_):
            self._load_()
            return
        for gene in gene_filtered:
            max_val = 0.0
            for die in parkinson:
                foo = graphForm[(graphForm['protein1'] == gene) & (graphForm['protein2'] == die)]
                if foo.empty:
                    pass
                else:
                    max_val = max(max_val, foo['score'].values[0])

            self.gene_value_pair[gene] = max_val
        self._save_()
        print(self.gene_value_pair)
        return self.gene_value_pair

    def filter(self, ratio):
        return {k: v for k, v in self.gene_value_pair.items() if v > ratio}

    def _save_(self):

        with open(self._path_, 'wb') as handle:
            pickle.dump(self.gene_value_pair, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def _load_(self):
        with open(self._path_, 'rb') as handle:
            self.gene_value_pair = pickle.load(handle)
