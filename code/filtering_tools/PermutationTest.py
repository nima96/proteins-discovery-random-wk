import os
import pickle

from rwr import rwr


class PermutationTest:
    def __init__(self, parkinson, graphForm, randomWalkR: rwr):
        self.parkinson = parkinson
        self.graphForm = graphForm
        self.pr_list = None
        self.__GRAPH__FILE = "../cache/permutation.data"
        if os.path.exists(self.__GRAPH__FILE):
            self.load_graph()
            return
        list_gene_ranks = randomWalkR.get_lists()
        list_gene_filtered = []
        for row in list_gene_ranks:
            new_row = {k: v for k, v in row.items() if v > 5e-05}
            list_gene_filtered.append(new_row)

        self.list_gene_filtered = list_gene_filtered

    def create_protein_list(self):
        if self.pr_list is not None:
            return self.pr_list
        for keys in self.graphForm['protein1']:
            key_artists = (1 for k in self.list_gene_filtered if k.get(keys))
            if sum(key_artists) / len(self.list_gene_filtered) > 0.05:
                for d in self.list_gene_filtered:
                    try:
                        del d[keys]
                    except KeyError:
                        pass

        print("create_protien_list")
        gene_filtered = set()
        for row in self.list_gene_filtered:
            new_row = {k for k, v in row.items()}
            gene_filtered = gene_filtered | new_row

        self.save_graph(gene_filtered)
        return gene_filtered

    def save_graph(self, pr_list):
        with open(self.__GRAPH__FILE, 'wb') as handle:
            pickle.dump(pr_list, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def load_graph(self):
        with open(self.__GRAPH__FILE, 'rb') as handle:
            self.pr_list = pickle.load(handle)
