import os

import pandas as pd
import pickle
from pathlib import Path


class EnsableConvertor:

    def __init__(self, path):
        self.__GRAPH__FILE = "../cache/convertor.txt"
        self.my_data = pd.read_csv(path,
                                   delimiter='	',
                                   low_memory=False,
                                   usecols=["protein_external_id",
                                            "preferred_name"
                                            ])
        self.convertor = bidict({})

    def create_convertor(self):
        self._create_convertor()

    def _create_convertor(self):
        for row in self.my_data.iterrows():
            self.convertor[row[1][0].split('.')[1]] = row[1][1]


    def convertEnsableToNormal(self, name):
        if len(name.split('.')) > 1:
            return self.convertor[name.split('.')[1]]
        return self.convertor[name]

    def convertNormalToEnsable(self, name):
        print(name)
        return self.convertor.inverse[name]

class bidict(dict):
    def __init__(self, *args, **kwargs):
        super(bidict, self).__init__(*args, **kwargs)
        self.inverse = {}
        for key, value in self.items():
            self.inverse.setdefault(value,[]).append(key)

    def __setitem__(self, key, value):
        if key in self:
            self.inverse[self[key]].remove(key)
        super(bidict, self).__setitem__(key, value)
        self.inverse.setdefault(value,[]).append(key)

    def __delitem__(self, key):
        self.inverse.setdefault(self[key],[]).remove(key)
        if self[key] in self.inverse and not self.inverse[self[key]]:
            del self.inverse[self[key]]
        super(bidict, self).__delitem__(key)