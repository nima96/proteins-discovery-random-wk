class CreateDiseaseDatabase:

    def __init__(self, data_path):
        self.data_path = data_path

    def read_data(self):
        self.disease_list = open(self.data_path, 'r').read().split("\n")

    def get_disease(self, disease_name):
        from pathlib import Path
        Path("../dataset/generated").mkdir(parents=True, exist_ok=True)

        y = open("../dataset/generated/%s.txt" % disease_name, 'w')
        genes = []
        gene_ids = []
        for line in self.disease_list:
            lineSeprated = line.split('\t')

            if len(lineSeprated) > 5 and disease_name in lineSeprated[5]:

                if lineSeprated[1] not in genes:
                    genes.append(lineSeprated[1])
                    gene_ids.append((lineSeprated[0].replace(" ", '')))

        y.write("\n".join(genes))
        y.close()
        return genes
