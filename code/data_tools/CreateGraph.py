import os

import pandas as pd
import networkx as nx
import pickle
from pathlib import Path


class CreateGraph:

    def __init__(self, path):
        self.__GRAPH__FILE = "../cache/graph.txt"
        self.my_data = pd.read_csv(path,
                                   delimiter=' ',
                                   low_memory=False,
                                   usecols=["protein1",
                                            "protein2",
                                            "combined_score"
                                            ])
        self.G = nx.Graph()

    def create_graph(self):
        if os.path.exists(self.__GRAPH__FILE):
            self.load_graph()
        else:
            self.create_edges()

    def create_edges(self):
        for row in self.my_data.iterrows():
            self.G.add_edge(row[1][0].split('.')[1], row[1][1], weight=row[1][2])

        self.save_graph()

    def return_graph(self):
        return self.G

    def save_graph(self):
        Path('../cache').mkdir(parents=True, exist_ok=True)
        with open(self.__GRAPH__FILE, 'wb') as handle:
            pickle.dump(self.G, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def load_graph(self):
        with open(self.__GRAPH__FILE, 'rb') as handle:
            self.G = pickle.load(handle)
