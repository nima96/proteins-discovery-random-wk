from data_tools.CreateGraph import CreateGraph
import pandas as pd
from l3 import ppiLPred


def get_neighbors(graph, node):
    neighbors = graph[node]
    _filtered_ = []
    for neighbor in list(neighbors):
        if neighbors[neighbor]['weight'] > 300:
            _filtered_.append(neighbor)
    return _filtered_


if __name__ == '__main__':
    # read gen-gen data
    my_data = pd.read_csv('../dataset/9606.protein.links.v11.0.txt',
                          delimiter=' ',
                          low_memory=False,
                          dtype=str
                          )
    ppi = []
    count = 0
    for row in my_data.iterrows():
        if int(row[1][2]) > 300:
            ppi.append([row[1][0], row[1][1]])
            count += 1
            print(count)
            if count == 300:
                break
    sortedPPI, sortedScores = ppiLPred.PPILinkPred(ppi)

    new_list = [a[0] + "," + a[1] + "," + str(b) for a, b in zip(sortedPPI, sortedScores)]

    with open("../cache/sorted_ppi.txt", "w") as txt_file:
        for line in new_list:
            txt_file.write(line + "\n")