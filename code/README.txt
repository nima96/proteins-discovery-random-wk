Files:
========================================
GraphNormalize.java : source code for column normalizing a graph
RandomWalk.java : source code for random walks with restarts on a graph
ExampleGraph.txt : example graph
ExampleGraph_normalized.txt : normalized version of the example graph (created using GraphNormalize)
startNodes1.txt, startNodes2.txt : sample starting nodes
sampleRandomWalkOutput1.txt, sampleRandomWalkOutput2.txt : sample outputs

Graph File Format:
========================================

<number of nodes in the graph>
<number of neighbors of first node> <name of first node> <index of first node>
<index of first neighbor of first node> <edge weight>
<index of second neighbor of first node> <edge weight>
....
....
<index of last neighbor of first node> <edge weight>
<number of neighbors of second node> <name of second node> <index of second node>
<index of first neighbor of second node> <edge weight>
<index of second neighbor of second node> <edge weight>
....
....
<index of last neighbor of first node> <edge weight>
.....
.....
.....
<number of neighbors of second node> <name of second node> <index of second node>
<index of first neighbor of second node> <edge weight>
<index of second neighbor of second node> <edge weight>
....
....
<index of last neighbor of first node> <edge weight>



Graph Normalization:
===========================================

The original network/graph file should be column normalized using GraphNormalize.class 
so that the RandomWalk method converges. 

  E.g.,
        #java GraphNormalize ExampleGraph.txt ExampleGraph_normalized.txt



Sample runs of RandomWalk.class:
============================================

#java RandomWalk ExampleGraph_normalized.txt startNodes1.txt 0.4 sampleRandomWalk1.txt
Graph reading completed.
12 nodes exist in the network.
Found 1 of the starting nodes in the graph.
Random Walk converged in 72 iterations.

#java RandomWalk ExampleGraph_normalized.txt startNodes2.txt 0.4 sampleRandomWalk2.txt
Graph reading completed.
12 nodes exist in the network.
Found 2 of the starting nodes in the graph.
Random Walk converged in 75 iterations.

The output of random walk is outputed descending order of proximity to the starting nodes. 
The affinity (i.e. proximity) values of the nodes are displayed in the second column. Note
that the total affinity adds up to 1.0 